const elements = require('./data.cjs');

function map(elements, cb) {
    const arr = [];

    if (!Array.isArray(elements) || map.arguments.length < 2 || cb === undefined) {
        return [];
    }

    for (let index = 0; index < elements.length; index++) {
        const eachElement = cb(elements[index], index, elements);
        arr.push(eachElement);
    }
    return(arr);
}

module.exports = map;


