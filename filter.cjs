const elements = require('./data.cjs');

function filter(elements, cb) {
    const arr = [];

    if (!Array.isArray(elements) || filter.arguments.length < 2) {
        return [];
    }

    for (let index = 0; index < elements.length; index++) {
        const result = cb(elements[index], index, elements);

        if (result === true) {
            arr.push(elements[index]);
        }
    }
    return arr;
}

module.exports = filter;

