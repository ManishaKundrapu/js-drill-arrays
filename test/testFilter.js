const data = require('../data.cjs');
const filter = require('../filter.cjs');

function cb(element, index, array) {
    return element > 2;
}

test('Testing Filter function', () => {
    expect(filter(data, cb)).toStrictEqual(data.filter((value, index, array) => value > 2));
}); 



