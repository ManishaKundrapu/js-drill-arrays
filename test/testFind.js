const data = require('../data.cjs');
const find = require('../find.cjs');

function cb(element) {
    return element > 4;
}

test('Testing Find function', () => {
    expect(find(data, cb)).toStrictEqual(data.find((value) => value > 4));
});
