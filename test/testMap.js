const data = require('../data.cjs');
const map = require('../map.cjs');

function cb(element, index, array) {
    return element;
}

test('Testing Map function', () => {
    expect(map(data, cb)).toStrictEqual(data.map((value, index, array) => value));
    expect(map(data, parseInt)).toStrictEqual(data.map(parseInt));
});


