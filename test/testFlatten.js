const data = require('../flattenData.cjs');
const flatten = require('../flatten.cjs');

test('Testing Flatten function', () => {
    expect(flatten(data,2)).toStrictEqual(data.flat(2));
});

