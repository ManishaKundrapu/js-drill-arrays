const elements = require('./flattenData.cjs');

function flatten(elements, depth = 1) {
    let result = [];
    depth = depth < 0 ? 0 : depth;

    if (!Array.isArray(elements) || flatten.arguments.length < 1) {
        return [];
    }
    if (depth == 0) {        
        for (let index = 0; index < elements.length; index++) {
            if (elements[index] !== undefined) {
                result.push(elements[index]);
            }
        }
    }
    else {

        for (let index = 0; index < elements.length; index++) {

            if (elements[index] === undefined) {
                continue;
            }
            if (Array.isArray(elements[index]) && depth !== 0) {
                result.push(...flatten(elements[index], depth - 1));
            }
            else {
                result.push(elements[index]);
            }
        }
    }
    return result;
}

module.exports = flatten;

