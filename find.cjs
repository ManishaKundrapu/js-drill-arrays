const elements = require('./data.cjs');

function find(elements, cb) {

    let result;

    if (!Array.isArray(elements) || find.arguments.length !== 2) {
        return [];
    }

    for (let index = 0; index < elements.length; index++) {
        result = cb(elements[index]);

        if (result === true) {

            return (elements[index]);

        }
    }
    return undefined;

};

module.exports = find;