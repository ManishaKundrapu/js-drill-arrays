const elements = require('./data.cjs');

function each(elements, cb) {

    let result = [];

    if (!Array.isArray(elements) || each.arguments.length !== 2) {
        return [];
    }


    for (let index = 0; index < elements.length; index++) {
        result.push(cb(elements[index], index));
    }

    return(result);
}

module.exports = each;
